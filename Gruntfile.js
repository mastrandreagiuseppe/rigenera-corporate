module.exports = function(grunt) {
  var modulesJs = [{
    name:'desktop',
    files:grunt.file.expand('js/init/*.js')
  }];

  var requirejsOptions = {};

  modulesJs.forEach(function(module) {
    module.files.forEach(function(file,i) {
      var completeFilename = file.split('/').pop(),
      filename = completeFilename.split('.').shift();

      if(filename !== 'libs'){
        requirejsOptions["["+module.name.toUpperCase()+"] -> "+filename]={
          options: {
            preserveLicenseComments: false,
            paths: {
              //'lodash':'empty:',
              //list file to empty
            },
            mainConfigFile: 'js/app/main_grunt.js',
            baseUrl: './js',
            name: 'init/'+filename,
            out: 'dist/init/'+completeFilename
          }
        };
      }else{
        requirejsOptions["[DESKTOP] -> libs"]={
          options: {
            preserveLicenseComments: false,
            mainConfigFile: 'js/app/main_grunt.js',
            baseUrl: './js',
            name: 'init/'+filename,
            out: 'dist/init/'+completeFilename
          }
        };
      }
    });
  });

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    requirejs: requirejsOptions,
    prompt: {
      createview: {
        options: {
          questions: [{
            config:  'targetdir',
            type:    'list',
            message: 'View target directory',
            choices: [
              {
                value: 'common',
                name:  'common'
              },
              {
                value: 'desktop',
                name:  'desktop'
              },
              {
                value: 'mobile',
                name:  'mobile'
              }
            ]
          },
          {
            config:  'viewname',
            type:    'input',
            message: 'View name'
          }
          ]
        }
      }
    },
    template:{
      'process-html-template': {
        options: {
          data: {
            targetdir : '<%= targetdir %>',
            viewname : '<%= viewname %>'
          }
        },
        files: { //targetdir viewname
          'project/apps/core/static/<%= targetdir %>/js/app/<%= viewname %>.js': ['project/apps/core/static/templates/app.js.tmpl'],
          'project/apps/core/static/<%= targetdir %>/js/views/<%= viewname %>.js': ['project/apps/core/static/templates/view.js.tmpl']
        }
      }
    },
    less:{
      options: {
        cleancss: true, // minify
        compress: true,
        plugins: [
          new (require('less-plugin-clean-css'))({keepSpecialComments:0})
        ]
      },
      desktop: {
        expand: true,
        cwd: 'project/apps/core/static/less',
        src: [
        '*.less'
        ],
        ext: '.css',
        dest: 'project/apps/core/static/css'
      },
      styles:{
        files: {
          "css/styles.css": "_less/styles.less"
        }
      }
    },
    autoprefixer: {
      options: {
        browsers: ['last 10 versions']
      },
      desktop: {
        expand: true,
        cwd: 'css/',
        src: [
        '*.css'
        ],
        ext: '.css',
        dest: 'css/'
      },
      styles:{
        files: {
          "css/styles.css": "css/styles.css"
        }
      }
    },
    watch: {
      default:{
        files: "project/apps/core/static/less/*.less",
        tasks: ["less", "autoprefixer"],
      },
      styles:{
        files: "_less/*.less",
        tasks: ["less:styles", "autoprefixer:styles"]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-prompt');
  grunt.loadNpmTasks('grunt-template');

  grunt.registerTask('default', ['watch']);
  grunt.registerTask('build', ['less', 'autoprefixer','requirejs']);
  grunt.registerTask('generateview', ['prompt','template']);

  grunt.registerTask('requirejs:all', function () {
    grunt.task.run('requirejs');
  });

  grunt.registerTask('requirejs:libs', function () {
    for (var key in grunt.config.data.requirejs) {
      if(key!=="[desktop] -> libs"){
        delete grunt.config.data.requirejs[key];
      }
    }
    grunt.task.run('requirejs');
  });

  grunt.registerTask('requirejs:modules', function () {
    for (var key in grunt.config.data.requirejs) {
      if(key==="[desktop] -> libs"){
        delete grunt.config.data.requirejs[key];
      }
    }
    grunt.task.run('requirejs');
  });

};