from django.utils.translation import ugettext_lazy as _

CMS_TEMPLATES = (
    ('cms_templates/home.html', _('Home')),
    ('cms_templates/generic.html', _('Generic')),
    ('cms_templates/about_us.html', _('About us')),
    ('cms_templates/partners.html', _('Partners')),
    ('cms_templates/sponsors.html', _('Sponsors')),
    ('cms_templates/about_us_partners.html', _('About us con partners')),
    ('cms_templates/contacts.html', _('Contacts')),
    ('cms_templates/temporarily_offline.html', _('Sito in manutenzione')),
)

CMS_TEMPLATE_INHERITANCE = True

'''
CMS_TEMPLATES_DIR: {
    1: '/absolute/path/for/site/1/',
    2: '/absolute/path/for/site/2/',
}

CMS_PLACEHOLDER_CONF = {
    None: {
        "plugins": ['TextPlugin'],
        'excluded_plugins': ['InheritPlugin'],
    }
    'content': {
        'plugins': ['TextPlugin', 'PicturePlugin'],
        'text_only_plugins': ['LinkPlugin'],
        'extra_context': {"width":640},
        'name': _("Content"),
        'language_fallback': True,
        'default_plugins': [
            {
                'plugin_type': 'TextPlugin',
                'values': {
                    'body':'<p>Lorem ipsum dolor sit amet...</p>',
                },
            },
        ],
        'child_classes': {
            'TextPlugin': ['PicturePlugin', 'LinkPlugin'],
        },
        'parent_classes': {
            'LinkPlugin': ['TextPlugin'],
        },
    },
    'right-column': {
        "plugins": ['TeaserPlugin', 'LinkPlugin'],
        "extra_context": {"width": 280},
        'name': _("Right Column"),
        'limits': {
            'global': 2,
            'TeaserPlugin': 1,
            'LinkPlugin': 1,
        },
        'plugin_modules': {
            'LinkPlugin': 'Extra',
        },
        'plugin_labels': {
            'LinkPlugin': 'Add a link',
        },
    },
    'base.html content': {
        "plugins": ['TextPlugin', 'PicturePlugin', 'TeaserPlugin'],
        'inherit': 'content',
    },
}

CMS_MEDIA_PATH = 'cms/'
CMS_MEDIA_ROOT = MEDIA_ROOT + CMS_MEDIA_PATH
CMS_MEDIA_URL = MEDIA_URL + CMS_MEDIA_PATH

CMS_PAGE_MEDIA_PATH = 'cms_page_media/'

'''

CMS_PLUGIN_CONTEXT_PROCESSORS = []
CMS_PLUGIN_PROCESSORS = []
CMS_APPHOOKS = (
    #'myapp.cms_app.MyApp',
    #'otherapp.cms_app.MyFancyApp',
    #'sampleapp.cms_app.SampleApp',
)

CKEDITOR_SETTINGS = {
}

CMS_LANGUAGES = {
    1: [
        {
            'code': 'it',
            'name': _('Italian'),
            'fallbacks': ['en'],
            'public': True,
            'hide_untranslated': True,
            'redirect_on_fallback':False,
        },
        {
            'code': 'en',
            'name': _('English'),
            'public': True,
            'hide_untranslated': True,
            'redirect_on_fallback': False,
        },
    ],
    'default': {
        'fallbacks': ['it', 'en', ],
        'redirect_on_fallback': True,
        'public': True,
        'hide_untranslated': True,
    }
}
