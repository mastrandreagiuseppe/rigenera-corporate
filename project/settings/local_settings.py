import os

DATABASES = {
    'default': {
        # Add 'postgresql_psycopg2', 'postgresql', 'mysql',
        # 'sqlite3' or 'oracle'.
        'ENGINE': 'django.db.backends.mysql',
        # Or path to database file if using sqlite3.
        'NAME': 'rigenera_corporate',
        # Not used with sqlite3.
        'USER': 'djangouser',
        # Not used with sqlite3.
        'PASSWORD': 'password',
        # Set to empty string for localhost. Not used with sqlite3.
        'HOST': '127.0.0.1',
        # Set to empty string for default. Not used with sqlite3.
        'PORT': '',
        # force to use InnoDB storage engine, only for mysql perche su questo abbiamo studiato ottimizzazioni
        'STORAGE_ENGINE': 'MYISAM',
        # 'OPTIONS': {"init_command": "SET storage_engine=MYISAM"},
    }
}

DEBUG = True

MEDIA_ROOT = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'media')
STATIC_ROOT = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'static')

ALLOWED_HOSTS = [
    '167.172.96.34',
    'rigeneralab.org',
    'www.rigeneralab.org',
]
