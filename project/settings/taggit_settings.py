def splitter(tag_string):
    return [t.strip().lower() for t in tag_string.split(',') if t.strip()]


TAGGIT_TAGS_FROM_STRING = 'project.settings.taggit_settings.splitter'

'''
def comma_joiner(tags):
    names = []
    for tag in tags:
        name = tag.name
        if ',' in name or ' ' in name:
            names.append('"%s"' % name)
        else:
            names.append(name)
    return names


TAGGIT_STRING_FROM_TAGS = 'project.settings.taggit_settings.comma_joiner'''