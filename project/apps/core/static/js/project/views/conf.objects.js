;(function(){
    var Models          = namespace('Project.Models'),
        Collections     = namespace('Project.Collections'),
        Routers         = namespace('Project.Routers'),
        Urls            = namespace('Project.Urls'),
        Vars            = namespace('Project.Vars'),
        Objects         = namespace('Project.Objects'),
        Views           = namespace('Project.Views');
    

    Objects.conf = {};

    Objects.conf['spectrumConfig'] = {
        showPaletteOnly: true,
        preferredFormat: "hex",
        togglePaletteOnly: true,
        hideAfterPaletteSelect: true,
        togglePaletteMoreText: 'more',
        togglePaletteLessText: 'less',
        color: 'blanchedalmond',
        palette: [
            ["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
            ["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
            ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
            ["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
            ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
            ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
            ["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
            ["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
        ]
    };

    Objects.conf['toastrConfig'] = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "preventDuplicates": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "7000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    moment.locale('it', {
        months : [
            "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio",
            "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"
        ],
        monthsShort : [
            "Gen", "Feb", "Mar", "Apr", "Mag", "Giu",
            "Lug", "Ago", "Set", "Ott", "Nov", "Dic"
        ],
        weekdaysShort : ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"],
        weekdaysMin: ['D', 'L', 'M', 'M', 'G', 'V', 'S'],

        longDateFormat : {
            LT : "HH:mm",
            LTS : "HH:mm:ss",
            L : "DD/MM/YYYY",
            LL : "D MMMM YYYY",
            LLL : "D MMMM YYYY LT",
            LLLL : "dddd D MMMM YYYY LT"
        },
        calendar : {
            sameDay: "[Oggi alle] LT",
            nextDay: '[Domani alle] LT',
            nextWeek: 'dddd [alle] LT',
            lastDay: '[Ieri alle] LT',
            lastWeek: 'dddd [alle] LT',
            sameElse: 'L'
        },
        relativeTime : {
            future: "fra %s",
            past:   "%s fa",
            s:  "secondi",
            m:  "un minuto",
            mm: "%d minuti",
            h:  "un'ora",
            hh: "%d ore",
            d:  "un giorno",
            dd: "%d giorni",
            M:  "un mese",
            MM: "%d mesi",
            y:  "un anno",
            yy: "%d anni"
        },
        //ordinalParse : /\d{1,2}(er|ème)/,
        //ordinal : function (number) {
        //    return number + (number === 1 ? 'er' : 'ème');
        //},
        meridiemParse: /PD|MD/,
        isPM: function (input) {
            return input.charAt(0) === 'M';
        },
        // in case the meridiem units are not separated around 12, then implement
        // this function (look at locale/id.js for an example)
        // meridiemHour : function (hour, meridiem) {
        //     return /* 0-23 hour, given meridiem token and hour 1-12 */
        // },
        meridiem : function (hours, minutes, isLower) {
            return hours < 12 ? 'AM' : 'PM';
        },
        //week : {
        //    dow : 1, // Monday is the first day of the week.
        //    doy : 4  // The week that contains Jan 4th is the first week of the year.
        //}
        
    });


    Objects.conf['errorMessages'] = {
        "A valid integer is required.": "Inserire un numero intero valido.",
        "This field may not be blank.": "Campo obbligatorio.",
        "A valid number is required.": "Inserire un numero valido.",
        "This field must be unique.": "Questo valore è già presente a db, specificare un altro valore."
    };

})();