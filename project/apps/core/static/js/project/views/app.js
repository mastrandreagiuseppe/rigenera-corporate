;(function(){
    var Models          = namespace('Project.Models'),
        Collections     = namespace('Project.Collections'),
        Routers         = namespace('Project.Routers'),
        Urls            = namespace('Project.Urls'),
        Vars            = namespace('Project.Vars'),
        Objects         = namespace('Project.Objects'),
        Views           = namespace('Project.Views');
    
    
    Views.AppView = Views.BaseView.extend({
        
        el: '#wrapper',
        
        events : {
            'click .js-logout': 'onLogoutBtnClick'
        },

        preInitialize : function(options){
            var me = this;
            me.channel = ChannelBroker.get("AppChannel");
            console.log("AppView preInitialize");
            me.user = options.user;            
            me.views = {};
            me.buildChildrenViews();
            me.initOptions(options);
        },

        initOptions: function(opts){
            var me = this;

            if(!!opts.router){
                me.router = opts.router;
                me.initRoutes();
            }

            if(!!opts.toastrConfig){
                toastr.options = toastrConfig;
            }else{
                toastr.options = Objects.conf.toastrConfig;
            }
        },

        initRoutes: function(){
            var me = this,
                app_router = me.router;
            console.log('initRoutes');
            

            app_router.on('route:dashboard', function () {
                console.log('router:dashboard');
                me.dashboardViewRender();
            });
            app_router.on('route:usersList', function () {
                console.log('router:usersList');

                if(!me.views.usersListView){
                    me.views.usersListView = new Views.UsersList({
                        store: new Collections.Users()
                    });
                }
                
                me.views.usersListView.store.fetch({reset: true});


                me.renderContent(me.views.usersListView, me.views.usersListView.el);
            });

            app_router.on('route:userDetail', function (id) {
                console.log('router:userDetail', id);

                if(!!id){
                    var detail = me.views.usersListView.store.findWhere({id: parseInt(id)});
                    if(!!detail){
                        m = new Models.User( detail.attributes );
                        me.views.userDetailView = new Views.UserDetail({ model: m });    
                        me.renderContent(me.views.userDetailView);
                    }
                }else{
                    me.views.userDetailView = new Views.UserDetail();
                    me.renderContent(me.views.userDetailView);
                }
            });

            app_router.on('route:mayorsList', function () {
                console.log('router:mayorsList');

                if(!me.views.mayorsListView){
                    me.views.mayorsListView = new Views.MayorsList({
                        store: new Collections.Mayors()
                    });
                }
                
                me.views.mayorsListView.store.fetch({reset: true});


                me.renderContent(me.views.mayorsListView, me.views.mayorsListView.el);
            });

            app_router.on('route:mayorDetail', function (id) {
                console.log('router:mayorDetail');

                me.mayorDetailViewRender(id);
            });

            app_router.on('route:pollingStationsList', function () {
                console.log('router:pollingStationsList');

                if(!me.views.pollingStationsListView){
                    me.views.pollingStationsListView = new Views.PollingStationsList({
                        store: new Collections.PollingStations()
                    });
                }
                
                me.views.pollingStationsListView.store.fetch({reset: true});


                me.renderContent(me.views.pollingStationsListView, me.views.pollingStationsListView.el);
            });

            app_router.on('route:pollingStationDetail', function (id) {
                console.log('router:pollingStationDetail');

                me.pollingStationDetailViewRender(id);
            });

            app_router.on('route:councilmenList', function () {
                console.log('router:councilmenList');

                if(!me.views.councilmenListView){
                    me.views.councilmenListView = new Views.CouncilmenList({
                        store: new Collections.Councilmen()
                    });
                }
                
                me.views.councilmenListView.store.fetch({reset: true});
                me.renderContent(me.views.councilmenListView, me.views.councilmenListView.el);
            });

            app_router.on('route:councilmanDetail', function (id) {
                console.log('router:councilmanDetail');

                me.councilmanDetailViewRender(id);
            });

            app_router.on('route:partiesList', function () {
                console.log('router:partiesList');

                if(!me.views.partiesListView){
                    me.views.partiesListView = new Views.PartiesList({
                        store: new Collections.Parties()
                    });
                }
                
                me.views.partiesListView.store.fetch({reset: true});


                me.renderContent(me.views.partiesListView, me.views.partiesListView.el);
            });

            app_router.on('route:partyDetail', function (id) {
                console.log('router:partyDetail');

                me.partyDetailViewRender(id);
            });


            app_router.on('route:myPollingStations', function () {
                console.log('router:myPollingStations');

                if(!me.views.myPollingStationsView){
                    me.views.myPollingStationsView = new Views.MyPollingStationsList({
                        store: new Collections.PollingStations()
                    });
                }
                
                me.views.myPollingStationsView.store.fetch({reset: true});
                me.renderContent(me.views.myPollingStationsView, me.views.myPollingStationsView.el);
            });

            app_router.on('route:myPollingStationsDetail', function (id) {
                console.log('router:myPollingStationsDetail');
                var partiesPS = new Collections.PartiesPS(),
                    councilmenPS = new Collections.CouncilmenPS(),
                    mayorsPS = new Collections.MayorsPS(),
                    parties = new Collections.Parties(),
                    councilmen = new Collections.Councilmen(),
                    mayors = new Collections.Mayors(),
                    pollingStation = new Models.PollingStation({id: parseInt(id)});
                
                $.when(
                    pollingStation.fetch(),
                    partiesPS.fetch({data: { ps_id: id }}),
                    councilmenPS.fetch({data: { ps_id: id  }}),
                    mayorsPS.fetch({data: { ps_id: id  }}),
                    parties.fetch(),
                    mayors.fetch(),
                    councilmen.fetch({data: { p_id: Vars.party_id }})).done(function(station, pPS, cPS, mPS, p, c, m){
                    
                    
                    me.views.myPollingStationView = new Views.MyPollingStation({
                        station       : station[0],
                        councilmenPS  : councilmenPS,
                        mayorsPS      : mayorsPS,
                        partiesPS     : partiesPS,
                        councilmen    : councilmen,
                        mayors        : mayors,
                        parties       : parties,
                        psId          : id
                    });


                    me.renderContent(me.views.myPollingStationView);
                });
            });

            app_router.on('route:simulation', function () {
                console.log('router:simulation');
                me.simulationViewRender();
            });

            app_router.on('route:secondBallot', function () {
                console.log('router:secondBallot');
                me.secondBallotViewRender();
            });

            app_router.on('route:secondBallotPublicView', function () {
                console.log('router:secondBallotPublicView');
                me.secondBallotPublicViewRender();
                setTimeout(function(){
                    $('#emptyWrapper', me.$el).append('<iframe src="http://elezioni.interno.it/referendum/scrutini/20161204/FX01000.htm" style="width:100%;min-height: 700px;"></iframe>');
                    $("#emptyWrapper").css('min-height', $("#emptyWrapper").height());
    
                }, 1500);
            });


            app_router.on('route:myProfile', function(){
                console.log('route:myProfile');
                me.views.myProfileView = new Views.MyProfile({ model: me.user });    
                me.renderContent(me.views.myProfileView);
            });
        },

        buildChildrenViews: function(){
            var me = this;
            me.menuView = new Views.MenuView({user: me.user});
            me.contentView = new Views.ContentView();
        },

        simulationViewRender: function(){
            var me = this,
                mayors = new Collections.Mayors(),
                councilmen = new Collections.Councilmen(), 
                parties = new Collections.Parties();
            $.when(
                mayors.fetch(),
                councilmen.fetch(),
                parties.fetch()).done(function(mayorsRequest, councilmenRequest, partiesRequest){
                me.views.simulationView = new Views.SimulationView({
                    mayors: mayors,
                    councilmen: councilmen,
                    parties: parties
                });
                me.renderContent(me.views.simulationView);
            });
        },

        dashboardViewRender: function(){
            var me = this,
                mayors = new Collections.Mayors(),
                councilmen = new Collections.Councilmen(), 
                parties = new Collections.Parties();
            $.when(
                mayors.fetch(),
                councilmen.fetch(),
                parties.fetch()).done(function(monthRes, yearsRes, roomsRes, todayRes){
                me.views.dashboardView = new Views.DashboardView({
                    mayors: mayors,
                    councilmen: councilmen,
                    parties: parties
                });
                me.renderContent(me.views.dashboardView);
            });
        },

        pollingStationDetailViewRender: function(id){
            var me = this,
                mayors = new Collections.MayorsPS(),
                councilmen = new Collections.CouncilmenPS(),
                parties = new Collections.PartiesPS(),
                model = new Models.PollingStation({id: parseInt(id)});

            $.when(
                mayors.fetch({data: {ps_id:id}}),
                councilmen.fetch({data: {ps_id:id}}),
                model.fetch(),
                parties.fetch({data: {ps_id:id}})).done(function(mayorsData, councilmenData, psData, partiesData){
                    me.views.pollingStationDetail = new Views.PollingStationDetail({
                        mayors: mayors,
                        councilmen: councilmen,
                        parties: parties,
                        model: model
                    });

                    me.renderContent(me.views.pollingStationDetail);
                
            });
        },

        mayorDetailViewRender: function(id){
            var me = this,
                ps = new Collections.MayorsPS(),
                model = new Models.Mayor({id: parseInt(id)});

            $.when(
                    model.fetch(),
                    ps.fetch({data: {m_id: id}})).done(function(m, psData){
                    me.views.mayorDetailView = new Views.MayorDetail({
                        pollingStations: ps,
                        model: model
                    });

                    me.renderContent(me.views.mayorDetailView);
            });
        },

        partyDetailViewRender: function(id){
            var me = this,
                ps = new Collections.PartiesPS(),
                model = new Models.Party({id: parseInt(id)});

            $.when(
                    model.fetch(),
                    ps.fetch({data: {p_id: id}})).done(function(m, psData){
                    me.views.partyDetailView = new Views.PartyDetail({
                        pollingStations: ps,
                        model: model
                    });

                    me.renderContent(me.views.partyDetailView);
            });
        },

        councilmanDetailViewRender: function(id){
            var me = this,
                ps = new Collections.CouncilmenPS(),
                model = new Models.Councilman({id: parseInt(id)});

            $.when(
                    model.fetch(),
                    ps.fetch({data: {p_id: id}})).done(function(m, psData){
                    me.views.councilmanDetailView = new Views.CouncilmanDetail({
                        pollingStations: ps,
                        model: model
                    });

                    me.renderContent(me.views.councilmanDetailView);
            });
        },

        secondBallotViewRender: function(){
            var me = this,
                mayorsPS = new Collections.MayorsPS(),
                mayors = new Collections.Mayors(),
                pollingStations = new Collections.PollingStations();
                
            $.when(
                pollingStations.fetch(),
                mayorsPS.fetch(),
                mayors.fetch()).done(function(stations, mPS, m){
                
                
                me.views.updateMayorsTable = new Views.MayorsUpdateTable({
                    stations      : pollingStations,
                    mayorsPS      : mayorsPS,
                    mayors        : mayors
                });


                me.renderContent(me.views.updateMayorsTable);
            });
        },

        secondBallotPublicViewRender: function(){
            var me = this,
                mayors = new Collections.Mayors();
                
            $.when(
                mayors.fetch()).done(function(stations, mPS, m){
                
                
                me.views.secondBallotPublicView = new Views.SecondBallotPublicView({
                    store        : mayors
                });


                me.renderContent(me.views.secondBallotPublicView);
            });
        },

        postRender: function(){
            var me = this;

            //Build children view
            me.$el.append(me.menuView.render().el);
            me.$el.append(me.contentView.render().el);
            if(Backbone.history.location.hash !== '#second_ballot'){
                Project.Objects.appView.router.navigate('#second_ballot', true);
            }else{
                me.secondBallotPublicViewRender();
                setTimeout(function(){
                    $('#emptyWrapper', me.$el).append('<iframe src="http://elezioni.interno.it/referendum/scrutini/20161204/FX01000.htm" style="width:100%;min-height: 700px;"></iframe>');
                    $("#emptyWrapper").css('min-height', $("#emptyWrapper").height());
    
                }, 1500);
                
            }
            
        },


        renderContentView: function(view){
            var me = this;
            
        },

        bindEvents : function(options) {
            var me = this;
            me.on('changeRoute', me.onChangeRoute, me);
        },

        renderContent: function(v, el){
            console.log(v);
            var me = this,
                el = el || v.render().el;
            
            $("#emptyWrapper").css('min-height', $("#emptyWrapper").height());
            
            if(!!me.activeView){
                me.activeView.detach();
            }

            me.activeView = v;

            $('#emptyWrapper', me.$el).html(el);

            if(v.templateId !== "#reservationDetailTpl"){
                v.delegateEvents();
            }
        },

        onChangeRoute: function(url){
            var me = this;
        },

        onLogoutBtnClick: function(e){
            var me = this;

            eraseCookie('auth_token');
            $.ajaxSetup({
                headers: {
                    'Authorization': ''
                }
            });
            window.location.href = Project.Urls.loginUrl;
        }
    });

    Views.ContentView = Views.BaseView.extend({
        templateId: '#contentViewTpl',
        className: 'gray-bg',
        attributes: {
            'id': 'page-wrapper'
        },

        postRender: function(){
            var me = this;
            me.fixHeight();
            $(window).bind("load resize scroll", function () {
                if (!$("body").hasClass('body-small')) {
                    me.fixHeight();
                }
            });
        },

        fixHeight: function() {
            var me = this,
                heightWithoutNavbar = $("body > #wrapper").height() - 61;
            $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");

            var navbarHeigh = $('nav.navbar-default').height();
            var wrapperHeigh = me.$el.height();

            if (navbarHeigh > wrapperHeigh) {
                me.$el.css("min-height", navbarHeigh + "px");
            }

            if (navbarHeigh < wrapperHeigh) {
                me.$el.css("min-height", $(window).height() + "px");
            }

            if ($('body').hasClass('fixed-nav')) {
                if (navbarHeigh > wrapperHeigh) {
                    me.$el.css("min-height", navbarHeigh - 60 + "px");
                } else {
                    me.$el.css("min-height", $(window).height() - 60 + "px");
                }
            }
        }
    });

})();

