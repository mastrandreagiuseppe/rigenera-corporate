;(function(){
    var Models          = namespace('Project.Models'),
        Collections     = namespace('Project.Collections'),
        Routers         = namespace('Project.Routers'),
        Urls            = namespace('Project.Urls'),
        Vars            = namespace('Project.Vars'),
        Objects         = namespace('Project.Objects'),
        Views           = namespace('Project.Views');
    
    
    Routers.Router = Backbone.Router.extend({

        routes: {
            '#': 'dashboard',
            'dashboard': 'dashboard',

            'users': 'usersList',
            'users/detail': 'userDetail',
            'users/detail/:id': 'userDetail',

            'mayors': 'mayorsList',
            'mayors/detail/:id': 'mayorDetail',
            
            'polling_stations': 'pollingStationsList',
            'polling_stations/detail/:id': 'pollingStationDetail',

            'parties': 'partiesList',
            'parties/detail/:id': 'partyDetail',

            'councilmen': 'councilmenList',
            'councilmen/detail/:id': 'councilmanDetail',

            'my-stats': 'myPollingStations',
            'my-stats/detail/:id': 'myPollingStationsDetail',

            'me': 'myProfile',

            'second-ballot-update': 'secondBallot' 
        }
    });
})();