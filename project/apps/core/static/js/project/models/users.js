;(function(){
    var Models          = namespace('Project.Models'),
        Collections     = namespace('Project.Collections'),
        Routers         = namespace('Project.Routers'),
        Urls            = namespace('Project.Urls'),
        Vars            = namespace('Project.Vars'),
        Objects         = namespace('Project.Objects'),
        Views           = namespace('Project.Views');
    
    Models.User = Models.BaseModel.extend({
        //idAttribute: 'slug',
        urlRoot:function (options){
            return Urls.usersListUrl;
        },
        fields: [
            {
                'name': 'user',
                'verboseName': 'Utente',
                required: true,
                fields: [
                    {
                        'name': 'first_name',
                        'verboseName': 'Nome'
                    },
                    {
                        'name': 'last_name',
                        'verboseName': 'Cognome'
                    },
                    {
                        'name': 'email',
                        'verboseName': 'E-mail',
                        required: true
                    },
                    {
                        'name': 'is_staff',
                        'verboseName': 'Staff'
                    },

                    {
                        'name': 'is_superuser',
                        'verboseName': 'Amministratore'
                    }
                ]
            }
        ]
    });

    Collections.Users = Collections.BaseStore.extend({
        models : Models.User,
        url : function (options){
            return Urls.usersListUrl;
        }
    });

})();