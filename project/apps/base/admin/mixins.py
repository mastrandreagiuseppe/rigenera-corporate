# -*- coding: utf-8 -*-
""" FHCoreLibs > ADMIN > Base.py by Frankhood
    @author: Frankhood Business Solutions
"""
from __future__ import absolute_import, unicode_literals

import copy
import os

from django.conf import settings
from django.contrib import admin, messages
from django.contrib.admin.filters import SimpleListFilter
from django.contrib.admin.views.main import ChangeList
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.utils import timezone
from django.utils.encoding import force_unicode
from django.utils.html import escape, escapejs
from django.utils.translation import ungettext, ugettext as _

#from .renderers import FHCSVRenderer
#from fhcore.utils.csv_utils import APIFileNameMixin


class OrderableModelAdmin(admin.ModelAdmin):
    order_field = 'order'
    ordering = ('order',)

    def __init__(self, *args, **kwargs):
        super(OrderableModelAdmin, self).__init__(*args, **kwargs)
        if self.order_field not in self.list_display:
            self.list_display = list(self.list_display) + [self.order_field]
        if self.order_field not in self.list_editable:
            self.list_editable = list(self.list_editable) + [self.order_field]

    def _media(self):
        js = (os.path.join(settings.STATIC_URL, 'js/bower_components/jquery/dist/jquery.min.js'),
              os.path.join(settings.STATIC_URL, 'js/bower_components/jquery-ui/jquery-ui.min.js'),
              os.path.join(settings.STATIC_URL, 'admin/js/orderable.js'))
        css = {'all': (os.path.join(settings.STATIC_URL, 'js/bower_components/jquery-ui/themes/base/jquery-ui.min.css'),)}
        currmedia = super(OrderableModelAdmin, self).media
        # currmedia = super(OrderableModelAdmin, self)._media() # active in django 1.3.x
        old_js = list(currmedia._js)
        old_js.extend(js)
        currmedia._js = tuple(old_js)
        old_css = list(currmedia._css.get('all', []))
        old_css.extend(css['all'])
        currmedia._css['all'] = old_css
        return currmedia
    media = property(_media)