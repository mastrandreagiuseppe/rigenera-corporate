# -*- coding: utf-8 -*-
""" ENTRIES MODULE > MANAGERS
    @author: Domenico Lamparelli <domenico@frankhood.it> 
    @author: Nicola Mosca <nico@frankhood.it>
"""
from __future__ import absolute_import, unicode_literals

import datetime
import logging

from django.conf import settings
from django.db import models
from django.db.models import Q
from django.db.models import Count
from django.utils import timezone

from django.contrib.sites.managers import CurrentSiteManager

from .settings import PUBLISHED_CODE

logger = logging.getLogger('project')

class CurrentSiteEntryManager(CurrentSiteManager):
    use_for_related_fields = True
    
    def published(self):
        """ get only the published entries """
        qs = super(CurrentSiteEntryManager, self).get_queryset()
        pub_start = Q(publication_start__lte= timezone.now()) 
        pub_end = (Q(publication_end__gte=timezone.now()) | Q(publication_end__isnull=True))
        status = Q(status=PUBLISHED_CODE)
        return qs.filter(pub_start & pub_end & status)


class PublicableModelManager(models.Manager):
    use_for_related_fields = True
    
    def get_query_set(self):
        return self.get_queryset()

    def get_queryset(self):
        """ get only the published entries """
        qs = super(PublicableModelManager, self).get_queryset()
        pub_start = Q(publication_start__lte= timezone.now()) 
        pub_end = (Q(publication_end__gte=timezone.now()) | Q(publication_end__isnull=True))
        status = Q(status=PUBLISHED_CODE)
        return qs.filter(pub_start & pub_end & status)

    def sort_latest_entries(self):
        qs = super(PublicableModelManager, self).get_queryset()
        return qs.order_by('-publication_start')
        
class PublishedEntryManager(PublicableModelManager):
    use_for_related_fields = True
    
    def search_for(self, search_string):
        qs = super(PublishedEntryManager, self).get_queryset()
        q_title = Q(title__icontains=search_string)
        q_content = Q(content__icontains=search_string)
        filtered_contents = qs.filter(q_title | q_content)
        return filtered_contents

    def sort_latest_entries(self):
        qs = super(PublishedEntryManager, self).get_queryset()
        return qs.order_by('-publication_start')
    
    def sort_most_commented(self):
        qs = super(PublishedEntryManager, self).get_queryset()
        qs = qs.annotate(num_comments=Count('comments'))
        qs = qs.order_by('-num_comments')
        return qs


class PublishedCategoryManager(PublicableModelManager):
    use_for_related_fields = True
    
    def search_for(self, search_string):
        qs = super(PublishedCategoryManager, self).get_queryset()
        q_name = Q(name__icontains=search_string)
        filtered_contents = qs.filter(q_name)
        return filtered_contents

    
class SoftDeleteModelManager(models.Manager):
    
    def get_query_set(self):
        return self.get_queryset()

    def get_queryset(self):
        qs = super(SoftDeleteModelManager, self).get_queryset()
        qs= qs.filter(is_removed=False)
        return qs 


class NotPreviewEntryManager(models.Manager):
    use_for_related_fields = True
    
    def get_query_set(self):
        return self.get_queryset()

    def get_queryset(self):
        """ get only the entries without preview """
        qs = super(NotPreviewEntryManager, self).get_queryset()
        is_not_preview = Q(is_preview=False)
        return qs.filter(is_not_preview)


class NotPreviewCategoryManager(models.Manager):        
    use_for_related_fields = True
    
    def get_query_set(self):
        return self.get_queryset()

    def get_queryset(self):
        """ get only the entries without preview """
        qs = super(NotPreviewCategoryManager, self).get_queryset()
        is_not_preview = Q(is_preview=False)
        return qs.filter(is_not_preview)


class NotPreviewPublishedEntryManager(NotPreviewCategoryManager, PublishedEntryManager):
    use_for_related_fields = True


class NotPreviewPublishedCategoryManager(NotPreviewCategoryManager, PublishedCategoryManager):
    use_for_related_fields = True


class SearchModelMixinManager(models.Manager):

    def search_for(self, search_string, lang=None):
        # http://www.nomadjourney.com/2009/04/dynamic-django-queries-with-kwargs/
        # nuova versione... adesso divido la stringa in piu stringhe....
        qs = self.get_queryset()
        search_array = [x.strip() for x in search_string.strip().split(' ') if len(x.strip()) > 3]
        # logger.info('search_array : %s'%(search_array,))
        if search_array:
            for search_str in search_array:
                qs = qs and qs.filter(**{'search_data__icontains': search_str})
                # logger.info('qs after  search_str %s : %s'%(search_str, qs))
            return qs.distinct()
        return qs.none()
