# -*- coding: utf-8 -*-

from django.conf.urls import include, url 
from .views.advanced import OrderingView
from .views.base import GenericSearchView

urlpatterns = [
	url(r'^api/save-orderable/$', OrderingView.as_view(), name='save-orderable-view'),
    url(r'^search/$', GenericSearchView.as_view(), name='search-view'),
]

'''
urlpatterns = patterns('',
    # autocomplete
    #url(r'^autocomplete/$', AutocompleteTag.as_view(), name='autocomplete-tag-view'),
    #url(r'^autocomplete_ctype/$', AutocompleteCtype.as_view(), name='autocomplete-ctype-view'),                      
    #url(r'^autocomplete_fk/(?P<app_label>[-\w]+)/(?P<model_name>[-\w]+)/$', AutocompleteForeignKey.as_view(), name='autocomplete-fk-view'),                      
    url(r'^api/save-orderable/$', OrderingView.as_view(), name='save-orderable-view'), 
    url(r'^search/$', GenericSearchView.as_view(), name='search-view'),                                      
)    
'''
