# -*- coding: utf-8 -*-
""" FHCoreLibs > Views by Frankhood
    @author: Frankhood Business Solutions
"""
import operator

from django import http
from django.apps import apps
from django.db import models
from django.db.models import Q
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponse, HttpResponseBadRequest
from django.utils.decorators import method_decorator
from django.utils.encoding import smart_str, force_unicode
from django.utils.translation import ugettext as _
from django.views.decorators.cache import never_cache
from django.views.generic.base import View

try:
    import json
except ImportError:
    from django.utils import simplejson as json

from ..exceptions import JsonException


class Autocomplete(View):
    """ Restituisce la lista degli oggetti di classe self.model il cui name inizia x term in funzione della parola mandatagli in ajax """
    model = None
    manager= 'objects'
    variable = None
    fk_variable = None
    filter_type = 'istartswith'
    slug_name = 'term'
    filter_slug_name = None
    
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        return super(Autocomplete, self).dispatch(request, *args, **kwargs)
    
    def get_term(self):
        term = self.request.GET.get(self.slug_name)
        return term
    
    def get_filter_term(self):
        filter_term = self.request.GET.get(self.filter_slug_name, '') 
        return filter_term
    
    def get_query_set(self, term, filter_term):
        filter_args = {'%s__%s' % (self.variable, self.filter_type) :term}
        return getattr(self.model,self.manager).select_related().filter(**filter_args)
    
    def build_data(self,qs):
        data = []
        for entry in qs:
            datadict = {'label': '%s' % getattr(entry,self.variable), 'value': '%s' % getattr(entry,self.variable)}
            data.append(datadict)
        return data
    
    def get(self, request, *args, **kwargs):
        if not request.is_ajax() and not request.GET.get('term'):
            return HttpResponseBadRequest('Request not accepted')
        term = self.get_term()
        filter_term = self.get_filter_term()
        qs = self.get_query_set(term, filter_term)
        data = self.build_data(qs)
        json_data = json.dumps(data)
        return HttpResponse(json_data, mimetype='application/json')

try:
    from tagging.models import Tag

    class AutocompleteTag(Autocomplete):
    
        model = Tag
        manager= 'objects'
        variable = 'name'
        fk_variable = 'items'
        filter_type = 'icontains'
        slug_name = 'term'
        
        def build_data(self,qs):
            data = []
            for entry in qs:
                datadict = {'label': '%s(%s)' % (getattr(entry,self.variable), getattr(entry,self.fk_variable).count()), 'value': u'%s' % getattr(entry,self.variable)}
                data.append(datadict)
            return data

except ImportError as ex:
    pass

class AutocompleteForeignKey(Autocomplete):
    
    model = None
    manager= 'objects'
    slug_name = 'term'
    
    def get_model(self):
        if self.model:
            return self.model
        else:
            app_label = self.kwargs.get('app_label',None)
            model_name = self.kwargs.get('model_name',None)
            if app_label and model_name:
                model = apps.get_model(app_label,model_name)
                if model:
                    return model
            raise ImproperlyConfigured("No model found with app_label {0} and model_name ".format(app_label,model_name))
    
    def get_query_set(self, term):
        if not self.model:
            raise ImproperlyConfigured("Model is required")
        if hasattr(self.model, self.manager):
            qs = getattr(self.model,self.manager).all()
        else:
            qs = self.model._default_manager.all()
        try:
            search = [models.Q(**{smart_str(item):smart_str(term)}) for item in self.model.autocomplete_search_fields()]
            qs = qs.filter(reduce(operator.or_, search))
        except AttributeError:
            raise JsonException('Non è possibile legare la tua entry a questo tipo di Content Type')
        return qs   
    
    def get_object(self,obj_id):
        return self.model._default_manager.filter(id=obj_id)
     
    def build_data(self,qs):
        data = []
        data = [{"value":f.pk,"label":u'%s' % force_unicode(f)} for f in qs[:30]]
        return data   
    
    def get_initial(self):
        initial = self.request.GET.get('initial',False)
        return initial
        
    def get(self, request, *args, **kwargs):
        if not request.is_ajax() and (not request.GET.get('term') and not request.GET.get('initial')):
            return HttpResponseBadRequest('Request not accepted')
        self.model = self.get_model()
        term = self.get_term()
        if self.get_initial():
            qs=self.get_object(term)
        else:
            try:
                qs = self.get_query_set(term)
            except JsonException,e:
                return HttpResponse(e.json_data, mimetype='application/json')
        data = self.build_data(qs)
        json_data = json.dumps(data)
        return HttpResponse(json_data, mimetype='application/json')


class AutocompleteCtype(Autocomplete):
    
    model = None
    manager= 'objects'
    slug_name = 'term'
    filter_slug_name = 'c_type'
    manager = 'not_preview_pub_objects'
    
    def get_model_by_ctype(self,c_type_id):
        try:
            ctype_obj = ContentType.objects.get(id=c_type_id)
        except ValueError:
            raise JsonException(_(u'Inserisci prima il content_type'))
        model = models.get_model(ctype_obj.app_label,ctype_obj.model)
        if not model:
            raise JsonException(_(u'Invalid Content_type'))
        return model
    
    def get_query_set(self, term, c_type_id):
        self.model = self.get_model_by_ctype(c_type_id)
        if hasattr(self.model, self.manager):
            qs = getattr(self.model,self.manager).all()
        else:
            qs = self.model._default_manager.all()
        try:
            search = [models.Q(**{smart_str(item):smart_str(term)}) for item in self.model.autocomplete_search_fields()]
            qs = qs.filter(reduce(operator.or_, search))
        except AttributeError:
            raise JsonException('Non è possibile legare la tua entry a questo tipo di Content Type')
        return qs   
    
    def get_object(self,obj_id, c_type_id):
        self.model = self.get_model_by_ctype(c_type_id)
        return self.model._default_manager.filter(id=obj_id)
     
    def build_data(self,qs):
        data = []
        data = [{"value":f.pk,"label":u'%s' % force_unicode(f)} for f in qs[:10]]
        return data   
    
    def get_initial(self):
        initial = self.request.GET.get('initial',False)
        return initial
        
    def get(self, request, *args, **kwargs):
        if not request.is_ajax() and (not request.GET.get('term') and not request.GET.get('initial')):
            return HttpResponseBadRequest('Request not accepted')
        term = self.get_term()
        filter_term = self.get_filter_term()
        if self.get_initial():
            qs=self.get_object(term, filter_term)
        else:
            try:
                qs = self.get_query_set(term, filter_term)
            except JsonException,e:
                return HttpResponse(e.json_data, mimetype='application/json')
        data = self.build_data(qs)
        json_data = json.dumps(data)
        return HttpResponse(json_data, mimetype='application/json')
    
class OrderingView(View):
 
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        return super(OrderingView, self).dispatch(request, *args, **kwargs)  

   
    def save_item(self,**kwargs):
        _c_type = ContentType.objects.get(app_label=kwargs['app_label'][5:], model=kwargs['app_model'])
        _object = _c_type.get_object_for_this_type(id=kwargs['obj_id'])
        setattr(_object, kwargs['order_field'], kwargs['new_order'])
        _object.save()
        return _c_type.model_class().objects.values('id', kwargs['order_field'])

    
    def get(self, request, *args, **kwargs):
        data= None
        if all([self.request.GET.has_key('order_field'),
                self.request.GET.has_key('app_label'),
                self.request.GET.has_key('app_model'),
                self.request.GET.has_key('obj_id'),
                self.request.GET.has_key('new_order')]):
            data = self.save_item(**(dict(order_field= self.request.GET.get('order_field'),
                                   app_label= self.request.GET.get('app_label'),
                                   app_model= self.request.GET.get('app_model'),
                                   obj_id=long(self.request.GET.get('obj_id')),
                                   new_order=long(self.request.GET.get('new_order'))
                                   )))
        json_data = json.dumps(list(data)) 
        return HttpResponse(json_data)       