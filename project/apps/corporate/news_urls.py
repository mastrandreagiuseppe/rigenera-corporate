# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import NewsDetailView, NewsCategoryListView, NewsCategoryListArchiveView, NewsListView

urlpatterns = [
    url(r'^(?P<year>[-\d]+)/(?P<month>[-\d]+)/$', NewsCategoryListArchiveView.as_view(), name='news-list'),
    url(r'^category/(?P<slug>[-\w]+)$', NewsCategoryListView.as_view(), name='news-category-list'),
    url(r'^$', NewsListView.as_view(), name='news-list'),
    url(r'^(?P<slug>[-\w]+)/$', NewsDetailView.as_view(), name='news-detail'),
]
