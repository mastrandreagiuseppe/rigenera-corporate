# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import EventDetailView, EventsCategoriesListView, EventCategoryView

urlpatterns = [
    url(r'^$', EventsCategoriesListView.as_view(), name='events-list'),
    url(r'^category/(?P<slug>[-\w]+)$', EventCategoryView.as_view(), name='events-category-list'),
    url(r'^(?P<slug>[-\w]+)/$', EventDetailView.as_view(), name='event-detail'),
]
