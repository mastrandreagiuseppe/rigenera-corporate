# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import MessageCreateAndSend

urlpatterns = [
	url(r'^send-mail$', MessageCreateAndSend.as_view(), name='send-message'),
]