# -*- coding: utf-8 -*-

import datetime

from project import settings

from django.core.mail import send_mail
from django.core.urlresolvers import reverse_lazy, reverse, NoReverseMatch
from django.http import JsonResponse
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.utils.translation import activate, get_language
from django.utils import timezone

from project.apps.base.mixins import AjaxableResponseMixin

from .models import NewsCategory, EventCategory, News, Room, Event, SentEmail

from menus.utils import set_language_changer


def getAlternateUrls(object):
    #iterate through all translated languages and get its url for alt lang meta tag                      
    cur_language = get_language()
    altUrls = {}
    for language in settings.LANGUAGES:
        try:
            code = language[0]
            activate(code)
            url = object.get_absolute_url()
            altUrls[code] = url
        finally:
            activate(cur_language)
    return altUrls;


class CorporateDetailView(DetailView):
    url = None

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        set_language_changer(self.request, self.get_absolute_url)
        response = super(CorporateDetailView, self).get(*args, **kwargs)
        return response

    def get_absolute_url(self, language=None):
        print self.object.get_absolute_url()
        return self.object.get_absolute_url()


class NewsDetailView(CorporateDetailView):

    template_name = 'corporate/news_detail.html'
    
    model = News

    def get_context_data(self, **kwargs):
        context = super(NewsDetailView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['alt_urls'] = getAlternateUrls(self.object)
        context['related'] = News.published_objects.exclude(id=self.object.id).exclude(slug__isnull=True).exclude(slug='')
        return context

class RoomDetailView(CorporateDetailView):
    url = 'rooms:room-detail'
    template_name = 'corporate/room_detail.html'

    model = Room

    def get_context_data(self, **kwargs):
        context = super(RoomDetailView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['alt_urls'] = getAlternateUrls(self.object)
        context['related'] = Room.published_objects.exclude(id=self.object.id).exclude(slug__isnull=True).exclude(slug='')
        context['promotions'] = Event.published_objects.filter(room__id=self.object.id).exclude(slug__isnull=True).exclude(slug='')
        return context

class EventDetailView(CorporateDetailView):
    url = 'events:event-detail'
    template_name = 'corporate/event_detail.html'

    model = Event

    def get_context_data(self, **kwargs):
        context = super(EventDetailView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['alt_urls'] = getAlternateUrls(self.object)
        context['categories'] = EventCategory.published_objects.all()
        context['related'] = Event.published_objects.exclude(id=self.object.id).exclude(slug__isnull=True).exclude(slug='').filter(event_start_date__gt=timezone.now())
        return context

class RoomsListView(ListView):
    url = 'rooms:rooms-list'
    template_name = 'corporate/rooms_list.html'
    paginate_by = 5
    model = Room

    def get_queryset(self):
        if self.request.GET.get('tags'):
            return Room.published_objects.filter(tags__name__icontains=self.request.GET.get('tags')).exclude(slug__isnull=True).exclude(slug='')
        else:
            return Room.published_objects.exclude(slug__isnull=True).exclude(slug='')

    def get_context_data(self, **kwargs):
        context = super(RoomsListView, self).get_context_data(**kwargs)
        if self.request.GET.get('tags'):
            context['tags'] = self.request.GET.get('tags')
        context['now'] = timezone.now()
        return context

class EventsCategoriesListView(ListView):
    template_name = 'corporate/events_categories_list.html'
    url = 'events:events-category-list'
    paginate_by = 50
    #model = EventCategory
    model = EventCategory

    def get_queryset(self):
        return EventCategory.published_objects.exclude(slug__isnull=True).exclude(slug='')

    def get_context_data(self, **kwargs):
        context = super(EventsCategoriesListView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

class EventListView(ListView):
    template_name = 'corporate/events_list.html'
    paginate_by = 5
    url = 'events:events-list'
    #model = EventCategory
    model = Event

    def get_queryset(self):
        if self.request.GET.get('tags'):
            return Event.published_objects.filter(tags__name__icontains=self.request.GET.get('tags')).exclude(slug__isnull=True).exclude(slug='')
        else:
            return Event.published_objects.exclude(slug__isnull=True).exclude(slug='')

    def get_context_data(self, **kwargs):
        context = super(EventListView, self).get_context_data(**kwargs)
        if self.request.GET.get('tags'):
            context['tags'] = self.request.GET.get('tags')
        if self.kwargs.get('slug', None):
            context['category_slug'] = self.kwargs.get('slug', None)
            context['object'] = EventCategory.published_objects.get(slug=self.kwargs.get('slug'))
        context['now'] = timezone.now()
        return context

class EventCategoryView(CorporateDetailView):
    url = 'events:events-list'
    template_name = 'corporate/events_list.html'
    model = EventCategory

    def get_context_data(self, **kwargs):
        context = super(EventCategoryView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['alt_urls'] = getAlternateUrls(self.object)
        context['object_list'] = Event.published_objects.filter(category=self.object)
        context['related'] = Event.published_objects.exclude(id=self.object.id).exclude(slug__isnull=True).exclude(slug='').filter(event_start_date__gt=timezone.now())
        return context

class NewsListView(ListView):
    url = 'news:news-list'
    template_name = 'corporate/news_list.html'
    paginate_by = 5
    model = News

    def get_queryset(self):
        if self.request.GET.get('tags'):
            return News.published_objects.filter(tags__name__icontains=self.request.GET.get('tags')).exclude(slug__isnull=True).exclude(slug='')
        else:
            return News.published_objects.all().exclude(slug__isnull=True).exclude(slug='')

    def get_context_data(self, **kwargs):
        context = super(NewsListView, self).get_context_data(**kwargs)
        if self.request.GET.get('tags'):
            context['tags'] = self.request.GET.get('tags')
        context['now'] = timezone.now()
        if self.kwargs.get('slug', None):
            context['category_slug'] = self.kwargs.get('slug', None)
            context['object'] = NewsCategory.objects.get(slug=self.kwargs.get('slug'))
        return context

class NewsCategoryListView(NewsListView):
    url = 'news:news-category-list'
    def get_queryset(self):
        category = self.kwargs.get('slug', None)
        if category:
            return Event.published_objects.filter(category__slug=category).exclude(slug__isnull=True).exclude(slug='')
        else:
            return Event.published_objects.all().exclude(slug__isnull=True).exclude(slug='')

class NewsCategoryListArchiveView(ListView):

    template_name = 'corporate/news_list.html'
    paginate_by = 5
    model = News

    def get_queryset(self):
        created = datetime.datetime.now()
        y = int(self.kwargs.get('year', created.year))
        m = int(self.kwargs.get('month', created.month))

        return News.published_objects.filter(created__year=y, created__month=m).exclude(slug__isnull=True).exclude(slug='')

    def get_context_data(self, **kwargs):
        context = super(NewsCategoryListArchiveView, self).get_context_data(**kwargs)
        try:
            now = datetime.datetime.now()
            y = int(self.kwargs.get('year', now.year))
            m = int(self.kwargs.get('month', now.month))
            created = datetime.datetime(y, m, 1)
            context['created'] = created

        except Exception:
            pass

        if self.request.GET.get('tags'):
            context['tags'] = self.request.GET.get('tags')
        context['now'] = timezone.now()
        return context


class MessageCreateAndSend(AjaxableResponseMixin, CreateView):
    model = SentEmail
    fields = ['subject', 'sender_email', 'body']
    success_url = '/test/'

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super(MessageCreateAndSend, self).form_valid(form)
        if self.request.is_ajax():
            data = {
                'pk': self.object.pk,
            }
            send_mail(self.object.subject, self.object.body, self.object.sender_email, ['info@rigeneralab.org'], fail_silently=False)
            return JsonResponse(data)
        else:
            return response


'''
class GenericSearchView(TemplateView):

    template_name = "corporate/search_results.html"

    def dispatch(self, request, *args, **kwargs):
        # Try to dispatch to the right method; if a method doesn't exist,
        # defer to the error handler. Also defer to the error handler if the
        # request method isn't on the approved list.
        if request.method.lower() in self.http_method_names:
            handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
        else:
            handler = self.http_method_not_allowed
        return handler(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.search =  request.POST.get("search", None)
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(GenericSearchView, self).get_context_data(**kwargs)

        context["search"] = self.search

        news = News.objects.filter(
            Q(title__icontains=self.search) | Q(content__icontains=self.search)
        ).filter(status=1)
        press_releases = PressRelease.objects.filter(
            Q(title__icontains=self.search) | Q(content__icontains=self.search)
        ).filter(status=1)
        downloads = Downloads.objects.filter(
            Q(title__icontains=self.search) | Q(content__icontains=self.search)
        ).filter(status=1)
        portfolio_items = PortfolioItem.objects.filter(
            Q(title__icontains=self.search) | Q(content__icontains=self.search)
        ).filter(status=1)

        context["news"] = news
        context["press_releases"] = press_releases
        context["downloads"] = downloads
        context["portfolio_items"] = portfolio_items

        context["count"] = news.count() + downloads.count() + press_releases.count() + portfolio_items.count()

        return context
'''

