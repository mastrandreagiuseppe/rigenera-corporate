from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

class RoomApphook(CMSApp):
    name = _("Rooms")

    app_name = 'rooms'

    def get_urls(self, page=None, language=None, **kwargs):
        return ["project.apps.corporate.rooms_urls"]       # replace this with the path to your application's URLs module

class EventAppHook(CMSApp):
    name = _("Events")

    app_name = 'events'

    def get_urls(self, page=None, language=None, **kwargs):
        return ["project.apps.corporate.events_urls"]       # replace this with the path to your application's URLs module


class NewsAppHook(CMSApp):
    name = _("News")

    app_name = 'news'

    def get_urls(self, page=None, language=None, **kwargs):
        return ["project.apps.corporate.news_urls"]       # replace this with the path to your application's URLs module 

apphook_pool.register(RoomApphook)
apphook_pool.register(EventAppHook)
apphook_pool.register(NewsAppHook)