# -*- coding: utf-8 -*-

import datetime

from django.db import models
from django.core.urlresolvers import reverse_lazy, reverse

from django.utils.translation import ugettext_lazy as _

from cms.models import CMSPlugin
from taggit.managers import TaggableManager

from photologue.models import Gallery, Photo

from project.apps.base.models import AdvancedCategory, AdvancedEntryModel
from project.apps.base.mixins import OrderableModelMixin


class NewsCategory(AdvancedCategory):
    cover_img = models.ForeignKey(Photo, related_name='news_category', blank=True, null=True, verbose_name=_('Cover'))
    class Meta(AdvancedCategory.Meta):
        verbose_name = _('News category')
        verbose_name_plural = _('News categories')

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        if self.slug == None: 
            #Per ora ritorniamo homepage... poi vediamo!
            return '/'
        else:
            return reverse('news:news-category-list', kwargs={'slug':self.slug})

class EventCategory(AdvancedCategory):
    cover_img = models.ForeignKey(Photo, related_name='event_category', blank=True, null=True, verbose_name=_('Cover'))
    fb_link = models.CharField(_("FB Link"), max_length=512, blank=True, null=True)
    class Meta(AdvancedCategory.Meta):
        verbose_name = _('Events Category')
        verbose_name_plural = _('Events Categories')

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        if self.slug == None: 
            #Per ora ritorniamo homepage... poi vediamo!
            return '/'
        else:
            return reverse('events:events-category-list', kwargs={'slug':self.slug})

class News(AdvancedEntryModel, OrderableModelMixin):
    gallery = models.ForeignKey(Gallery, related_name='news', blank=True, null=True, verbose_name=_("Gallery"))
    cover_img = models.ForeignKey(Photo, related_name='news', blank=True, null=True, verbose_name=_('Cover'))
    category = models.ForeignKey(NewsCategory, related_name='news', blank=True, null=True, verbose_name=_('Category'))
    # This is the important bit - where we add in the tags.
    tags = TaggableManager(blank=True, verbose_name=_("Tags"))
    fb_link = models.CharField(_("FB Link"), max_length=512, blank=True, null=True)

    class Meta(AdvancedEntryModel.Meta):
        verbose_name = _('News')
        verbose_name_plural = _('News')
        ordering = ['order',]

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        if self.slug == None: 
            #Per ora ritorniamo homepage... poi vediamo!
            return '/'
        else:
            return reverse('news:news-detail', kwargs={'slug':self.slug})

class Room(AdvancedEntryModel, OrderableModelMixin):
    gallery = models.ForeignKey(Gallery, related_name='room', blank=True, null=True, verbose_name=_("Gallery"))
    cover_img = models.ForeignKey(Photo, related_name='room', blank=True, null=True, verbose_name=_('Cover'))
    # This is the important bit - where we add in the tags.
    tags = TaggableManager(blank=True, verbose_name=_("Tags"))
    fb_link = models.CharField(_("FB Link"), max_length=512, blank=True, null=True)

    class Meta(AdvancedEntryModel.Meta):
        verbose_name = _('Room')
        verbose_name_plural = _('Rooms')
        ordering = ['order',]

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        if self.slug == None: 
            #Per ora ritorniamo homepage... poi vediamo!
            return '/'
        else:
            return reverse('rooms:room-detail', kwargs={'slug':self.slug})


class Event(AdvancedEntryModel, OrderableModelMixin):
    gallery = models.ForeignKey(Gallery, related_name='event', blank=True, null=True, verbose_name=_("Gallery"))
    cover_img = models.ForeignKey(Photo, related_name='event', blank=True, null=True, verbose_name=_('Cover'))
    category = models.ForeignKey(EventCategory, related_name='news', blank=True, null=True, verbose_name=_('Category'))
    event_start_date = models.DateTimeField(_("Event start date"), blank=True, null=True)
    event_end_date = models.DateTimeField(_("Event end date"), blank=True, null=True)
    event_link = models.CharField(_("FB Link"), max_length=512, blank=True, null=True)
    room = models.ForeignKey(Room, related_name='room', blank=True, null=True, verbose_name=_('Promo'))
    tags = TaggableManager(blank=True, verbose_name=_("Tags"))
    rooms = models.ManyToManyField(
        Room,
        related_name="rooms_events",
        related_query_name="%(app_label)s_%(class)ss",
        verbose_name=_('Rooms'),
        blank=True
    )
    class Meta(AdvancedEntryModel.Meta):
        verbose_name = _('Event')
        verbose_name_plural = _('Events')
        ordering = ['order',]

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        if self.slug == None: 
            #Per ora ritorniamo homepage... poi vediamo!
            return '/'
        else:
            return reverse('events:event-detail', kwargs={'slug':self.slug})

class SentEmail(models.Model):
    sender_email = models.EmailField(_("Sender Email"))
    created = models.DateTimeField(_("modified at"), auto_now=True)
    subject = models.CharField(_("Subject"), max_length=255)
    body = models.TextField(_("Body"))

    class Meta:
        verbose_name = _('Sent Email')
        verbose_name_plural = _('Sent Emails')
        ordering = ['-created']
        get_latest_by = '-created'

    def __unicode__(self):
        return self.subject

class GenericContentPluginModel(CMSPlugin, OrderableModelMixin):
    created = models.DateTimeField(_("created at"), auto_now_add=True, blank=True, null=True)
    modified = models.DateTimeField(_("modified at"), auto_now=True, blank=True, null=True)
    created_by = models.CharField(_('created by'), max_length=155, blank=True, editable=False)
    modified_by = models.CharField(_('modified by'), max_length=155, blank=True, editable=False)
    title = models.CharField(_("title"), max_length=155, blank=True, null=True)
    slug = models.SlugField(_('slug'), max_length=155, blank=True, editable=False)
    subtitle = models.CharField(_("subtitle"), max_length=155, blank=True)
    content = models.TextField(_("content"), blank=True)
    excerpt = models.TextField(_("abstract"), blank=True)
    fb_link = models.URLField(_('Facebook'), blank=True, null=True)
    twitter_link = models.URLField(_('Twitter'), blank=True, null=True)
    linkedin_link = models.URLField(_('Linkedin'), blank=True, null=True)
    instagram_link = models.URLField(_('Instagram'), blank=True, null=True)
    email = models.EmailField(_('Email'), blank=True, null=True)
    cover_img = models.ImageField(upload_to='uploads/generic', verbose_name=_("Cover"), blank=True, null=True)
    gallery = models.ForeignKey(Gallery, related_name='generic_content', blank=True, null=True, verbose_name=_("Gallery"))

    def copy_relations(self, oldinstance):
        self.cover_img = oldinstance.cover_img
        self.gallery = oldinstance.gallery

    def __unicode__(self):
        return self.title
