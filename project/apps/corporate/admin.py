# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import NewsCategory, News, Event, EventCategory, Room, SentEmail
from .forms import NewsAdminForm, NewsCategoryForm, EventForm, RoomForm, EventCategoryForm
from modeltranslation.admin import TranslationAdmin

from project.apps.base.admin.mixins import OrderableModelAdmin

from tinymce.widgets import TinyMCE


class ModelTranslationTabMixin(object):
    class Media:
        js = (
            'http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }

class BaseTranslationAdmin(TranslationAdmin, ModelTranslationTabMixin, OrderableModelAdmin):
    list_editable = ('status', 'order', )
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name.startswith('content_') or db_field.name.startswith('excerpt_'):
            kwargs['widget'] = TinyMCE(attrs={'cols': 80, 'rows': 30})
        return super(BaseTranslationAdmin, self).formfield_for_dbfield(db_field, **kwargs)


class NewsAdmin(BaseTranslationAdmin):
    list_display = ('title', 'status', 'subtitle', 'slug' , 'order', )
    form = NewsAdminForm
    fields = (('title', 'subtitle', 'slug'), ('status', 'publication_start', 'publication_end'), 'content', 'excerpt', ('cover_img', 'gallery'), ('category', 'tags'),)
    prepopulated_fields = {"slug": ("title",)}

class NewsCategoryAdmin(BaseTranslationAdmin):
    list_display = ('name', 'status', 'slug' , 'order', )
    form = NewsCategoryForm
    fields = (('name', 'slug'), ('status', ), 'content', 'excerpt', ('cover_img', ) )
    prepopulated_fields = {"slug": ("name",)}


class EventAdmin(BaseTranslationAdmin):
    list_display = ('title', 'status', 'subtitle', 'slug' , 'order', )
    form = EventForm
    fields = (('title', 'subtitle', 'slug'), ('status', 'event_start_date', 'event_end_date', 'rooms'), ('room'), 'content', 'excerpt', ('cover_img', 'gallery'), ('category', 'tags'), 'event_link')
    prepopulated_fields = {"slug": ("title",)}

class RoomAdmin(BaseTranslationAdmin):
    list_display = ('title', 'status', 'subtitle', 'slug' , 'order', )
    form = RoomForm
    fields = (('title', 'subtitle', 'slug'), ('status', 'publication_start', 'publication_end'), 'content', 'excerpt', ('cover_img', 'gallery'), 'tags', 'fb_link')
    prepopulated_fields = {"slug": ("title",)}

class EventCategoryAdmin(BaseTranslationAdmin):
    list_display = ('name', 'status', 'slug' , 'order', )
    form = EventCategoryForm
    fields = (('name', 'slug'), ('status', ), 'content', ('cover_img', ), 'fb_link' )
    prepopulated_fields = {"slug": ("name",)}

admin.site.register(News, NewsAdmin)
admin.site.register(NewsCategory, NewsCategoryAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(EventCategory, EventCategoryAdmin)
admin.site.register(Room, RoomAdmin)
admin.site.register(SentEmail)
