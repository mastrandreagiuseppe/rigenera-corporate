# -*- coding: utf-8 -*-

from django import forms
from django.contrib import admin
from tinymce.widgets import TinyMCE

from .models import NewsCategory, News, Event, EventCategory, Room, GenericContentPluginModel

class NewsAdminForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    excerpt = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    class Meta:
        model = News
        fields = '__all__'

class EventForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    excerpt = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    class Meta:
        model = Event
        fields = '__all__'

class NewsCategoryForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    excerpt = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    class Meta:
        model = NewsCategory
        fields = '__all__'

class EventCategoryForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    excerpt = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    class Meta:
        model = EventCategory
        fields = '__all__'

class RoomForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    excerpt = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    class Meta:
        model = Room
        fields = '__all__'


class PartnerPluginForm(forms.ModelForm):
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'cover_img', 'fb_link', 'twitter_link', 'linkedin_link', 'instagram_link', 'email']

class CoverPluginForm(forms.ModelForm):
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'subtitle', 'cover_img' ]

class GalleryPluginForm(forms.ModelForm):
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'gallery' ]

class StaffPluginForm(forms.ModelForm):
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'subtitle', 'cover_img', 'content', 'fb_link', 'twitter_link', 'linkedin_link', 'instagram_link', 'email']

class SponsorPluginForm(forms.ModelForm):
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'cover_img', 'fb_link', 'twitter_link', 'linkedin_link', 'instagram_link', 'email']


class CustomTextForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=False)
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'content',]

class GenericHeaderForm(forms.ModelForm):
    class Meta:
        model = GenericContentPluginModel
        fields = ['title', 'subtitle',]
