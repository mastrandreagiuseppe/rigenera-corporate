# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import RoomDetailView, RoomsListView

urlpatterns = [
    url(r'^$', RoomsListView.as_view(), name='rooms-list'),
    url(r'^(?P<slug>[-\w]+)/$', RoomDetailView.as_view(), name='room-detail'),
]