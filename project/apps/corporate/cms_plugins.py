from django.utils.translation import ugettext as _
from django.contrib import admin

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .models import GenericContentPluginModel
from .forms import PartnerPluginForm, StaffPluginForm, CoverPluginForm, GalleryPluginForm, SponsorPluginForm, CustomTextForm, GenericHeaderForm

class StaffPluginPublisher(CMSPluginBase):
    model = GenericContentPluginModel  # model where plugin data are saved
    module = _("About us Page")
    name = _("Staff Plugin")  # name of the plugin in the interface
    render_template = "cms_templates/plugins/staff_plugin.html"
    form = StaffPluginForm

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

class NumberPluginPublisher(CMSPluginBase):
    model = GenericContentPluginModel  # model where plugin data are saved
    module = _("About us Page")
    name = _("Animated Number Plugin")  # name of the plugin in the interface
    render_template = "cms_templates/plugins/number_plugin.html"

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

class ServicePluginPublisher(CMSPluginBase):
    model = GenericContentPluginModel  # model where plugin data are saved
    module = _("About us Page")
    name = _("Service Plugin")  # name of the plugin in the interface
    render_template = "cms_templates/plugins/service_plugin.html"
    form = PartnerPluginForm

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

class PartnerPluginPublisher(CMSPluginBase):
    model = GenericContentPluginModel  # model where plugin data are saved
    module = _("About us Page")
    name = _("Partner Plugin")  # name of the plugin in the interface
    render_template = "cms_templates/plugins/partner_plugin.html"
    form = PartnerPluginForm

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

class CoverPluginPublisher(CMSPluginBase):
    model = GenericContentPluginModel  # model where plugin data are saved
    module = _("Generic Page")
    name = _("Cover Image Plugin")  # name of the plugin in the interface
    render_template = "cms_templates/plugins/top_cover_plugin.html"
    form = CoverPluginForm

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

class ContactsPluginPublisher(CMSPluginBase):
    model = GenericContentPluginModel  # model where plugin data are saved
    module = _("Generic Page")
    name = _("Contacts with background image")  # name of the plugin in the interface
    render_template = "cms_templates/plugins/contacts_plugin.html"
    form = CoverPluginForm

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

class GalleryPluginPublisher(CMSPluginBase):
    model = GenericContentPluginModel  # model where plugin data are saved
    module = _("Gallery")
    name = _("Images slideshow")  # name of the plugin in the interface
    render_template = "cms_templates/plugins/gallery.html"
    form = GalleryPluginForm

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

class SponsorPluginPublisher(CMSPluginBase):
    model = GenericContentPluginModel  # model where plugin data are saved
    module = _("About us Page")
    name = _("Sponsor")  # name of the plugin in the interface
    render_template = "cms_templates/plugins/sponsor.html"
    form = SponsorPluginForm

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

class InstitutionalSponsorPluginPublisher(CMSPluginBase):
    model = GenericContentPluginModel  # model where plugin data are saved
    module = _("Generic Page")
    name = _("Cover image without title")  # name of the plugin in the interface
    render_template = "cms_templates/plugins/top_cover_without_title.html"
    form = CoverPluginForm

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

class CustomTextPlugin(CMSPluginBase):
    model = GenericContentPluginModel  # model where plugin data are saved
    module = _("Generic Text")
    name = _("Generic Text Plugin")  # name of the plugin in the interface
    render_template = "cms_templates/plugins/generic_text.html"
    form = CustomTextForm

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

class GenericHeaderPlugin(CMSPluginBase):
    model = GenericContentPluginModel  # model where plugin data are saved
    module = _("Generic Text")
    name = _("Generic Header")  # name of the plugin in the interface
    render_template = "cms_templates/plugins/generic_content_header.html"
    form = GenericHeaderForm

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

plugin_pool.register_plugin(StaffPluginPublisher)
plugin_pool.register_plugin(NumberPluginPublisher)
plugin_pool.register_plugin(ServicePluginPublisher)
plugin_pool.register_plugin(PartnerPluginPublisher)
plugin_pool.register_plugin(CoverPluginPublisher)
plugin_pool.register_plugin(ContactsPluginPublisher)
plugin_pool.register_plugin(GalleryPluginPublisher)
plugin_pool.register_plugin(SponsorPluginPublisher)
plugin_pool.register_plugin(InstitutionalSponsorPluginPublisher)
plugin_pool.register_plugin(CustomTextPlugin)
plugin_pool.register_plugin(GenericHeaderPlugin)


