# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import project.apps.base.fields
from django.conf import settings
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('taggit', '0002_auto_20150616_2121'),
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('photologue', '0010_auto_20160105_1307'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', project.apps.base.fields.FixedSlugField(blank=True)),
                ('slug_it', project.apps.base.fields.FixedSlugField(null=True, blank=True)),
                ('slug_en', project.apps.base.fields.FixedSlugField(null=True, blank=True)),
                ('status', models.IntegerField(default=0, verbose_name='status', choices=[(0, 'draft'), (99, 'published')])),
                ('publication_start', models.DateTimeField(null=True, verbose_name='publication start', blank=True)),
                ('publication_end', models.DateTimeField(null=True, verbose_name='publication end', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='modified at')),
                ('created_by', models.CharField(verbose_name='created by', max_length=155, editable=False, blank=True)),
                ('modified_by', models.CharField(verbose_name='modified by', max_length=155, editable=False, blank=True)),
                ('title', models.CharField(max_length=155, verbose_name='title', blank=True)),
                ('title_it', models.CharField(max_length=155, null=True, verbose_name='title', blank=True)),
                ('title_en', models.CharField(max_length=155, null=True, verbose_name='title', blank=True)),
                ('subtitle', models.CharField(max_length=155, verbose_name='subtitle', blank=True)),
                ('subtitle_it', models.CharField(max_length=155, null=True, verbose_name='subtitle', blank=True)),
                ('subtitle_en', models.CharField(max_length=155, null=True, verbose_name='subtitle', blank=True)),
                ('content', models.TextField(verbose_name='content', blank=True)),
                ('content_it', models.TextField(null=True, verbose_name='content', blank=True)),
                ('content_en', models.TextField(null=True, verbose_name='content', blank=True)),
                ('excerpt', models.TextField(verbose_name='abstract', blank=True)),
                ('excerpt_it', models.TextField(null=True, verbose_name='abstract', blank=True)),
                ('excerpt_en', models.TextField(null=True, verbose_name='abstract', blank=True)),
                ('object_id', models.PositiveIntegerField(null=True, blank=True)),
                ('event_start_date', models.DateTimeField(null=True, verbose_name='Event start date', blank=True)),
                ('event_end_date', models.DateTimeField(null=True, verbose_name='Event end date', blank=True)),
                ('event_link', models.CharField(max_length=512, null=True, verbose_name='FB Link', blank=True)),
                ('author', models.ForeignKey(related_name='event_written', verbose_name='author', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ['-publication_start', '-created'],
                'abstract': False,
                'get_latest_by': 'publication_start',
                'verbose_name': 'Event',
                'verbose_name_plural': 'Events',
            },
        ),
        migrations.CreateModel(
            name='EventCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', project.apps.base.fields.FixedSlugField(blank=True)),
                ('slug_it', project.apps.base.fields.FixedSlugField(null=True, blank=True)),
                ('slug_en', project.apps.base.fields.FixedSlugField(null=True, blank=True)),
                ('status', models.IntegerField(default=0, verbose_name='status', choices=[(0, 'draft'), (99, 'published')])),
                ('publication_start', models.DateTimeField(null=True, verbose_name='publication start', blank=True)),
                ('publication_end', models.DateTimeField(null=True, verbose_name='publication end', blank=True)),
                ('order', models.PositiveIntegerField(default=0, null=True, verbose_name='position', db_index=True, blank=True)),
                ('lang', models.CharField(max_length=5, verbose_name='language', choices=[(b'it', 'Italian'), (b'en', 'English')])),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='modified at')),
                ('created_by', models.CharField(verbose_name='created by', max_length=155, editable=False, blank=True)),
                ('modified_by', models.CharField(verbose_name='modified by', max_length=155, editable=False, blank=True)),
                ('name', models.CharField(max_length=155, verbose_name='name')),
                ('name_it', models.CharField(max_length=155, null=True, verbose_name='name')),
                ('name_en', models.CharField(max_length=155, null=True, verbose_name='name')),
                ('css_class', models.CharField(max_length=32, verbose_name='CSS class', blank=True)),
                ('content', models.TextField(verbose_name='content', blank=True)),
                ('excerpt', models.TextField(verbose_name='abstract', blank=True)),
                ('cover_img', models.ForeignKey(related_name='event_category', verbose_name='Cover', blank=True, to='photologue.Photo', null=True)),
            ],
            options={
                'ordering': ['order'],
                'abstract': False,
                'verbose_name': 'Events Category',
                'verbose_name_plural': 'Events Categories',
            },
        ),
        migrations.CreateModel(
            name='GenericContentPluginModel',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='corporate_genericcontentpluginmodel', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created at', null=True)),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='modified at', null=True)),
                ('created_by', models.CharField(verbose_name='created by', max_length=155, editable=False, blank=True)),
                ('modified_by', models.CharField(verbose_name='modified by', max_length=155, editable=False, blank=True)),
                ('title', models.CharField(max_length=155, null=True, verbose_name='title', blank=True)),
                ('slug', models.SlugField(verbose_name='slug', max_length=155, editable=False, blank=True)),
                ('subtitle', models.CharField(max_length=155, verbose_name='subtitle', blank=True)),
                ('content', models.TextField(verbose_name='content', blank=True)),
                ('excerpt', models.TextField(verbose_name='abstract', blank=True)),
                ('fb_link', models.URLField(null=True, verbose_name='Facebook', blank=True)),
                ('twitter_link', models.URLField(null=True, verbose_name='Twitter', blank=True)),
                ('linkedin_link', models.URLField(null=True, verbose_name='Linkedin', blank=True)),
                ('instagram_link', models.URLField(null=True, verbose_name='Instagram', blank=True)),
                ('cover_img', models.ImageField(upload_to=b'uploads/generic', null=True, verbose_name='Cover', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', project.apps.base.fields.FixedSlugField(blank=True)),
                ('slug_it', project.apps.base.fields.FixedSlugField(null=True, blank=True)),
                ('slug_en', project.apps.base.fields.FixedSlugField(null=True, blank=True)),
                ('status', models.IntegerField(default=0, verbose_name='status', choices=[(0, 'draft'), (99, 'published')])),
                ('publication_start', models.DateTimeField(null=True, verbose_name='publication start', blank=True)),
                ('publication_end', models.DateTimeField(null=True, verbose_name='publication end', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='modified at')),
                ('created_by', models.CharField(verbose_name='created by', max_length=155, editable=False, blank=True)),
                ('modified_by', models.CharField(verbose_name='modified by', max_length=155, editable=False, blank=True)),
                ('title', models.CharField(max_length=155, verbose_name='title', blank=True)),
                ('title_it', models.CharField(max_length=155, null=True, verbose_name='title', blank=True)),
                ('title_en', models.CharField(max_length=155, null=True, verbose_name='title', blank=True)),
                ('subtitle', models.CharField(max_length=155, verbose_name='subtitle', blank=True)),
                ('subtitle_it', models.CharField(max_length=155, null=True, verbose_name='subtitle', blank=True)),
                ('subtitle_en', models.CharField(max_length=155, null=True, verbose_name='subtitle', blank=True)),
                ('content', models.TextField(verbose_name='content', blank=True)),
                ('content_it', models.TextField(null=True, verbose_name='content', blank=True)),
                ('content_en', models.TextField(null=True, verbose_name='content', blank=True)),
                ('excerpt', models.TextField(verbose_name='abstract', blank=True)),
                ('excerpt_it', models.TextField(null=True, verbose_name='abstract', blank=True)),
                ('excerpt_en', models.TextField(null=True, verbose_name='abstract', blank=True)),
                ('object_id', models.PositiveIntegerField(null=True, blank=True)),
                ('fb_link', models.CharField(max_length=512, null=True, verbose_name='FB Link', blank=True)),
                ('author', models.ForeignKey(related_name='news_written', verbose_name='author', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ['-publication_start', '-created'],
                'abstract': False,
                'get_latest_by': 'publication_start',
                'verbose_name': 'News',
                'verbose_name_plural': 'News',
            },
        ),
        migrations.CreateModel(
            name='NewsCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', project.apps.base.fields.FixedSlugField(blank=True)),
                ('slug_it', project.apps.base.fields.FixedSlugField(null=True, blank=True)),
                ('slug_en', project.apps.base.fields.FixedSlugField(null=True, blank=True)),
                ('status', models.IntegerField(default=0, verbose_name='status', choices=[(0, 'draft'), (99, 'published')])),
                ('publication_start', models.DateTimeField(null=True, verbose_name='publication start', blank=True)),
                ('publication_end', models.DateTimeField(null=True, verbose_name='publication end', blank=True)),
                ('order', models.PositiveIntegerField(default=0, null=True, verbose_name='position', db_index=True, blank=True)),
                ('lang', models.CharField(max_length=5, verbose_name='language', choices=[(b'it', 'Italian'), (b'en', 'English')])),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='modified at')),
                ('created_by', models.CharField(verbose_name='created by', max_length=155, editable=False, blank=True)),
                ('modified_by', models.CharField(verbose_name='modified by', max_length=155, editable=False, blank=True)),
                ('name', models.CharField(max_length=155, verbose_name='name')),
                ('name_it', models.CharField(max_length=155, null=True, verbose_name='name')),
                ('name_en', models.CharField(max_length=155, null=True, verbose_name='name')),
                ('css_class', models.CharField(max_length=32, verbose_name='CSS class', blank=True)),
                ('content', models.TextField(verbose_name='content', blank=True)),
                ('excerpt', models.TextField(verbose_name='abstract', blank=True)),
                ('cover_img', models.ForeignKey(related_name='news_category', verbose_name='Cover', blank=True, to='photologue.Photo', null=True)),
            ],
            options={
                'ordering': ['order'],
                'abstract': False,
                'verbose_name': 'News category',
                'verbose_name_plural': 'News categories',
            },
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', project.apps.base.fields.FixedSlugField(blank=True)),
                ('slug_it', project.apps.base.fields.FixedSlugField(null=True, blank=True)),
                ('slug_en', project.apps.base.fields.FixedSlugField(null=True, blank=True)),
                ('status', models.IntegerField(default=0, verbose_name='status', choices=[(0, 'draft'), (99, 'published')])),
                ('publication_start', models.DateTimeField(null=True, verbose_name='publication start', blank=True)),
                ('publication_end', models.DateTimeField(null=True, verbose_name='publication end', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='modified at')),
                ('created_by', models.CharField(verbose_name='created by', max_length=155, editable=False, blank=True)),
                ('modified_by', models.CharField(verbose_name='modified by', max_length=155, editable=False, blank=True)),
                ('title', models.CharField(max_length=155, verbose_name='title', blank=True)),
                ('title_it', models.CharField(max_length=155, null=True, verbose_name='title', blank=True)),
                ('title_en', models.CharField(max_length=155, null=True, verbose_name='title', blank=True)),
                ('subtitle', models.CharField(max_length=155, verbose_name='subtitle', blank=True)),
                ('subtitle_it', models.CharField(max_length=155, null=True, verbose_name='subtitle', blank=True)),
                ('subtitle_en', models.CharField(max_length=155, null=True, verbose_name='subtitle', blank=True)),
                ('content', models.TextField(verbose_name='content', blank=True)),
                ('content_it', models.TextField(null=True, verbose_name='content', blank=True)),
                ('content_en', models.TextField(null=True, verbose_name='content', blank=True)),
                ('excerpt', models.TextField(verbose_name='abstract', blank=True)),
                ('excerpt_it', models.TextField(null=True, verbose_name='abstract', blank=True)),
                ('excerpt_en', models.TextField(null=True, verbose_name='abstract', blank=True)),
                ('object_id', models.PositiveIntegerField(null=True, blank=True)),
                ('fb_link', models.CharField(max_length=512, null=True, verbose_name='FB Link', blank=True)),
                ('author', models.ForeignKey(related_name='room_written', verbose_name='author', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('content_type', models.ForeignKey(blank=True, to='contenttypes.ContentType', null=True)),
                ('cover_img', models.ForeignKey(related_name='room', verbose_name='Cover', blank=True, to='photologue.Photo', null=True)),
                ('gallery', models.ForeignKey(related_name='room', verbose_name='Gallery', blank=True, to='photologue.Gallery', null=True)),
                ('tags', taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', blank=True, help_text='A comma-separated list of tags.', verbose_name='Tags')),
            ],
            options={
                'ordering': ['-publication_start', '-created'],
                'abstract': False,
                'get_latest_by': 'publication_start',
                'verbose_name': 'Room',
                'verbose_name_plural': 'Rooms',
            },
        ),
        migrations.AddField(
            model_name='news',
            name='category',
            field=models.ForeignKey(related_name='news', verbose_name='Category', blank=True, to='corporate.NewsCategory', null=True),
        ),
        migrations.AddField(
            model_name='news',
            name='content_type',
            field=models.ForeignKey(blank=True, to='contenttypes.ContentType', null=True),
        ),
        migrations.AddField(
            model_name='news',
            name='cover_img',
            field=models.ForeignKey(related_name='news', verbose_name='Cover', blank=True, to='photologue.Photo', null=True),
        ),
        migrations.AddField(
            model_name='news',
            name='gallery',
            field=models.ForeignKey(related_name='news', verbose_name='Gallery', blank=True, to='photologue.Gallery', null=True),
        ),
        migrations.AddField(
            model_name='news',
            name='tags',
            field=taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', blank=True, help_text='A comma-separated list of tags.', verbose_name='Tags'),
        ),
        migrations.AddField(
            model_name='event',
            name='category',
            field=models.ForeignKey(related_name='news', verbose_name='Category', blank=True, to='corporate.EventCategory', null=True),
        ),
        migrations.AddField(
            model_name='event',
            name='content_type',
            field=models.ForeignKey(blank=True, to='contenttypes.ContentType', null=True),
        ),
        migrations.AddField(
            model_name='event',
            name='cover_img',
            field=models.ForeignKey(related_name='event', verbose_name='Cover', blank=True, to='photologue.Photo', null=True),
        ),
        migrations.AddField(
            model_name='event',
            name='gallery',
            field=models.ForeignKey(related_name='event', verbose_name='Gallery', blank=True, to='photologue.Gallery', null=True),
        ),
        migrations.AddField(
            model_name='event',
            name='room',
            field=models.ForeignKey(related_name='room', verbose_name='Promo', blank=True, to='corporate.Room', null=True),
        ),
        migrations.AddField(
            model_name='event',
            name='rooms',
            field=models.ManyToManyField(related_query_name=b'%(app_label)s_%(class)ss', related_name='rooms_events', verbose_name='Rooms', to='corporate.Room', blank=True),
        ),
        migrations.AddField(
            model_name='event',
            name='tags',
            field=taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', blank=True, help_text='A comma-separated list of tags.', verbose_name='Tags'),
        ),
    ]
