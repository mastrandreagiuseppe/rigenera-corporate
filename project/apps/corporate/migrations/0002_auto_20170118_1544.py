# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('corporate', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='eventcategory',
            name='content_en',
            field=models.TextField(null=True, verbose_name='content', blank=True),
        ),
        migrations.AddField(
            model_name='eventcategory',
            name='content_it',
            field=models.TextField(null=True, verbose_name='content', blank=True),
        ),
        migrations.AddField(
            model_name='eventcategory',
            name='excerpt_en',
            field=models.TextField(null=True, verbose_name='abstract', blank=True),
        ),
        migrations.AddField(
            model_name='eventcategory',
            name='excerpt_it',
            field=models.TextField(null=True, verbose_name='abstract', blank=True),
        ),
        migrations.AddField(
            model_name='newscategory',
            name='content_en',
            field=models.TextField(null=True, verbose_name='content', blank=True),
        ),
        migrations.AddField(
            model_name='newscategory',
            name='content_it',
            field=models.TextField(null=True, verbose_name='content', blank=True),
        ),
        migrations.AddField(
            model_name='newscategory',
            name='excerpt_en',
            field=models.TextField(null=True, verbose_name='abstract', blank=True),
        ),
        migrations.AddField(
            model_name='newscategory',
            name='excerpt_it',
            field=models.TextField(null=True, verbose_name='abstract', blank=True),
        ),
        migrations.AlterField(
            model_name='eventcategory',
            name='name',
            field=models.CharField(max_length=155, verbose_name='name', blank=True),
        ),
        migrations.AlterField(
            model_name='eventcategory',
            name='name_en',
            field=models.CharField(max_length=155, null=True, verbose_name='name', blank=True),
        ),
        migrations.AlterField(
            model_name='eventcategory',
            name='name_it',
            field=models.CharField(max_length=155, null=True, verbose_name='name', blank=True),
        ),
        migrations.AlterField(
            model_name='newscategory',
            name='name',
            field=models.CharField(max_length=155, verbose_name='name', blank=True),
        ),
        migrations.AlterField(
            model_name='newscategory',
            name='name_en',
            field=models.CharField(max_length=155, null=True, verbose_name='name', blank=True),
        ),
        migrations.AlterField(
            model_name='newscategory',
            name='name_it',
            field=models.CharField(max_length=155, null=True, verbose_name='name', blank=True),
        ),
    ]
