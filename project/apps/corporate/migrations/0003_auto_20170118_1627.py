# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import project.apps.base.fields


class Migration(migrations.Migration):

    dependencies = [
        ('corporate', '0002_auto_20170118_1544'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='slug',
            field=project.apps.base.fields.FixedSlugField(),
        ),
        migrations.AlterField(
            model_name='event',
            name='slug_en',
            field=project.apps.base.fields.FixedSlugField(null=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='slug_it',
            field=project.apps.base.fields.FixedSlugField(null=True),
        ),
        migrations.AlterField(
            model_name='eventcategory',
            name='slug',
            field=project.apps.base.fields.FixedSlugField(),
        ),
        migrations.AlterField(
            model_name='eventcategory',
            name='slug_en',
            field=project.apps.base.fields.FixedSlugField(null=True),
        ),
        migrations.AlterField(
            model_name='eventcategory',
            name='slug_it',
            field=project.apps.base.fields.FixedSlugField(null=True),
        ),
        migrations.AlterField(
            model_name='news',
            name='slug',
            field=project.apps.base.fields.FixedSlugField(),
        ),
        migrations.AlterField(
            model_name='news',
            name='slug_en',
            field=project.apps.base.fields.FixedSlugField(null=True),
        ),
        migrations.AlterField(
            model_name='news',
            name='slug_it',
            field=project.apps.base.fields.FixedSlugField(null=True),
        ),
        migrations.AlterField(
            model_name='newscategory',
            name='slug',
            field=project.apps.base.fields.FixedSlugField(),
        ),
        migrations.AlterField(
            model_name='newscategory',
            name='slug_en',
            field=project.apps.base.fields.FixedSlugField(null=True),
        ),
        migrations.AlterField(
            model_name='newscategory',
            name='slug_it',
            field=project.apps.base.fields.FixedSlugField(null=True),
        ),
        migrations.AlterField(
            model_name='room',
            name='slug',
            field=project.apps.base.fields.FixedSlugField(),
        ),
        migrations.AlterField(
            model_name='room',
            name='slug_en',
            field=project.apps.base.fields.FixedSlugField(null=True),
        ),
        migrations.AlterField(
            model_name='room',
            name='slug_it',
            field=project.apps.base.fields.FixedSlugField(null=True),
        ),
    ]
