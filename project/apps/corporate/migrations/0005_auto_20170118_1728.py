# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('corporate', '0004_auto_20170118_1632'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='order',
            field=models.PositiveIntegerField(default=0, null=True, verbose_name='position', db_index=True, blank=True),
        ),
        migrations.AddField(
            model_name='genericcontentpluginmodel',
            name='order',
            field=models.PositiveIntegerField(default=0, null=True, verbose_name='position', db_index=True, blank=True),
        ),
        migrations.AddField(
            model_name='news',
            name='order',
            field=models.PositiveIntegerField(default=0, null=True, verbose_name='position', db_index=True, blank=True),
        ),
        migrations.AddField(
            model_name='room',
            name='order',
            field=models.PositiveIntegerField(default=0, null=True, verbose_name='position', db_index=True, blank=True),
        ),
    ]
