# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('corporate', '0007_auto_20170223_1047'),
    ]

    operations = [
        migrations.AddField(
            model_name='genericcontentpluginmodel',
            name='email',
            field=models.EmailField(max_length=254, null=True, verbose_name='Email', blank=True),
        ),
    ]
