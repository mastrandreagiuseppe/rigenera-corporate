# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('corporate', '0008_genericcontentpluginmodel_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='eventcategory',
            name='fb_link',
            field=models.CharField(max_length=512, null=True, verbose_name='FB Link', blank=True),
        ),
    ]
