# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('photologue', '0010_auto_20160105_1307'),
        ('corporate', '0006_auto_20170209_1530'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sentemail',
            options={'ordering': ['-created'], 'get_latest_by': '-created', 'verbose_name': 'Sent Email', 'verbose_name_plural': 'Sent Emails'},
        ),
        migrations.AddField(
            model_name='genericcontentpluginmodel',
            name='gallery',
            field=models.ForeignKey(related_name='generic_content', verbose_name='Gallery', blank=True, to='photologue.Gallery', null=True),
        ),
    ]
