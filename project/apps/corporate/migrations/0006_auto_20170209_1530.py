# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('corporate', '0005_auto_20170118_1728'),
    ]

    operations = [
        migrations.CreateModel(
            name='SentEmail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sender_email', models.EmailField(max_length=254, verbose_name='Sender Email')),
                ('created', models.DateTimeField(auto_now=True, verbose_name='modified at')),
                ('subject', models.CharField(max_length=255, verbose_name='Subject')),
                ('body', models.TextField(verbose_name='Body')),
            ],
            options={
                'ordering': ['-created'],
                'get_latest_by': '-created',
            },
        ),
        migrations.AlterModelOptions(
            name='event',
            options={'ordering': ['order'], 'get_latest_by': 'publication_start', 'verbose_name': 'Event', 'verbose_name_plural': 'Events'},
        ),
        migrations.AlterModelOptions(
            name='news',
            options={'ordering': ['order'], 'get_latest_by': 'publication_start', 'verbose_name': 'News', 'verbose_name_plural': 'News'},
        ),
        migrations.AlterModelOptions(
            name='room',
            options={'ordering': ['order'], 'get_latest_by': 'publication_start', 'verbose_name': 'Room', 'verbose_name_plural': 'Rooms'},
        ),
    ]
