# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static

from django.contrib import admin
from django.views.generic import TemplateView

from filebrowser.sites import site

from rest_framework.authtoken import views

from project import settings

admin_patterns = [
    # Examples:
    # url(r'^$', 'reservations.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
    #url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/?', include(admin.site.urls)),
    url(r'^', include('project.apps.base.urls')),
]

urlpatterns = [
    url(r'^api-token-auth/', views.obtain_auth_token)
]


urlpatterns += [
    url(r'^photologue/', include('photologue.urls', namespace='photologue')),
]

if settings.DEBUG:
    pass
    # urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


urlpatterns += [
    url(r'^tinymce/', include('tinymce.urls')),
]

urlpatterns += [
    url(r'^corporate/', include('project.apps.corporate.urls', namespace='corporate', app_name='corporate')), #Questa in realtà la inseriremo dagli apphook
]

cms_patterns = [
    #url(r'^/?$', TemplateView.as_view(template_name="templates/home.html"), name='homepage_view'),
    url(r'^', include('cms.urls')),
]

urlpatterns += i18n_patterns(
    #url(r'^events/(?P<slug>[-\w]+)/$', EventDetailView.as_view(), name='event-detail'),
    url(r'^', include(admin_patterns, namespace='')),
    url(r'^', include(cms_patterns, namespace='')),
)
